# EGG_data_file_generator

A software that generates EGG data file that are compatible with and can be imported to OpenBCI_GUI software.

Very simple C code that generates a fake EEG signal (customizable), to be read in OpenBCI GUI.

The code has been tested with 200Hz sampling rate (dt=5ms).
